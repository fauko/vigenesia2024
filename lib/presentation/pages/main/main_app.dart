import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';
import 'package:vigenesia2024/helper/shared_pref.dart';
import 'package:vigenesia2024/presentation/pages/auth/sign_in.dart';
import 'package:vigenesia2024/presentation/pages/main/home/home.dart';

class MainApp extends StatefulWidget {
  const MainApp({super.key});

  @override
  State<MainApp> createState() => _MainAppState();
}

class _MainAppState extends State<MainApp> {
  String token = '';
  @override
  void initState() {
    // SharedPrefApp().prefRemoveUser();
    SharedPrefApp().prefLoadUser().then((value) {
      setState(() {
        value != null ? token = value[0] : token = "kosong";
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Sizer(builder: (context, orientation, deviceType) {
      return MaterialApp(
        debugShowCheckedModeBanner: false,
        debugShowMaterialGrid: false,
        home: !token.contains('kosong') ? const HomeApp() : const SignInPage(),
      );
    });
  }
}
