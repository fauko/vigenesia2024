import 'dart:io';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:logger/logger.dart';
import 'package:sizer/sizer.dart';
import 'package:vigenesia2024/data/configure_url.dart';
import 'package:vigenesia2024/data/motivasi.dart';
import 'package:vigenesia2024/domain/motivasi/motivasi.dart';
import 'package:vigenesia2024/helper/navigate.dart';
import 'package:vigenesia2024/helper/snackbar.dart';
import 'package:vigenesia2024/presentation/pages/main/home/my_post.dart';
import 'package:vigenesia2024/presentation/widget/showleftshet.dart';

class EditMotivasi extends StatefulWidget {
  const EditMotivasi({super.key, required this.idmotiv});
  final String idmotiv;
  @override
  State<EditMotivasi> createState() => _EditMotivasiState();
}

class _EditMotivasiState extends State<EditMotivasi> {
  List<File> files = [];

  final fetchmotiv = MotivasiData();
  List<Motivasi> modelmotivasi = [];
  Future<void> _pickFile() async {
    final result = await FilePicker.platform.pickFiles(
      allowMultiple: true,
      type: FileType.custom,
      allowedExtensions: ['png', 'jpg', 'jpeg'],
      // allowedExtensions: ['xlsx', 'xls', 'pdf' , ],
    );

    if (result != null) {
      setState(() {
        files = result.paths.map((path) => File(path!)).toList();
      });
    }
  }

  @override
  void initState() {
    fetchmotiv.getDataUser();
    Future.delayed(Duration.zero, () {
      fetchmotiv.motivasiStream.listen((event) {
        setState(() {
          modelmotivasi = event;
        });
      });
    });
    Future.delayed(const Duration(seconds: 5), () {
      fetchmotiv.dispose();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    TextEditingController isimotivasi = TextEditingController(
        text: modelmotivasi.isNotEmpty
            ? modelmotivasi
                .where((element) => element.idmotivasi == widget.idmotiv)
                .first
                .isimotivasi
            : '');
    TextEditingController judul = TextEditingController(
        text: modelmotivasi.isNotEmpty
            ? modelmotivasi
                .where((element) => element.idmotivasi == widget.idmotiv)
                .first
                .judul
            : '');
    TextEditingController tags = TextEditingController(
        text: modelmotivasi.isNotEmpty
            ? modelmotivasi
                .where((element) => element.idmotivasi == widget.idmotiv)
                .first
                .tags
            : '');
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        leading: Row(
          children: [
            IconButton(
                onPressed: () {
                  SnackBarPopUp().showLeftSheet(context, const ShowLeftSheet());
                },
                icon: Icon(
                  Icons.menu,
                  size: 25.sp,
                ))
          ],
        ),
      ),
      body: modelmotivasi.isNotEmpty
          ? SizedBox(
              width: MediaQuery.sizeOf(context).width,
              height: MediaQuery.sizeOf(context).height,
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    Form(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Judul:",
                            style: GoogleFonts.rubik(
                              fontSize: 12.sp,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          Container(
                            height: 40.sp,
                            width: 200.sp,
                            margin: const EdgeInsets.only(top: 10),
                            padding: const EdgeInsets.symmetric(horizontal: 20),
                            decoration: BoxDecoration(
                              boxShadow: const [
                                BoxShadow(
                                    color: Colors.black26,
                                    offset: Offset(0, 1),
                                    blurRadius: 6.0)
                              ],
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(10.0),
                            ),
                            child: TextFormField(
                              controller: judul,
                              decoration: InputDecoration(
                                //   prefixIcon: prefixIcon,
                                border: InputBorder.none,
                                focusedBorder: InputBorder.none,
                                enabledBorder: InputBorder.none,
                                errorBorder: InputBorder.none,
                                disabledBorder: InputBorder.none,
                                hintText: '',
                                hintStyle: TextStyle(fontSize: 10.sp),
                                contentPadding: EdgeInsets.only(left: 5.sp),
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 20.sp,
                          ),
                          Text(
                            "Isi:",
                            style: GoogleFonts.rubik(
                              fontSize: 12.sp,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          Container(
                            height: 150.sp,
                            width: 200.sp,
                            margin: const EdgeInsets.only(top: 10),
                            padding: const EdgeInsets.symmetric(horizontal: 20),
                            decoration: BoxDecoration(
                              boxShadow: const [
                                BoxShadow(
                                    color: Colors.black26,
                                    offset: Offset(0, 1),
                                    blurRadius: 6.0)
                              ],
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(10.0),
                            ),
                            child: TextFormField(
                              controller: isimotivasi,
                              decoration: InputDecoration(
                                //   prefixIcon: prefixIcon,
                                border: InputBorder.none,
                                focusedBorder: InputBorder.none,
                                enabledBorder: InputBorder.none,
                                errorBorder: InputBorder.none,
                                disabledBorder: InputBorder.none,
                                hintText: '',
                                hintStyle: TextStyle(fontSize: 10.sp),
                                contentPadding: EdgeInsets.only(left: 5.sp),
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 20.sp,
                          ),
                          Text(
                            "Tags#:",
                            style: GoogleFonts.rubik(
                              fontSize: 12.sp,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          Container(
                            height: 40.sp,
                            width: 200.sp,
                            margin: const EdgeInsets.only(top: 10),
                            padding: const EdgeInsets.symmetric(horizontal: 20),
                            decoration: BoxDecoration(
                              boxShadow: const [
                                BoxShadow(
                                    color: Colors.black26,
                                    offset: Offset(0, 1),
                                    blurRadius: 6.0)
                              ],
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(10.0),
                            ),
                            child: TextFormField(
                              controller: tags,
                              decoration: InputDecoration(
                                //   prefixIcon: prefixIcon,
                                border: InputBorder.none,
                                focusedBorder: InputBorder.none,
                                enabledBorder: InputBorder.none,
                                errorBorder: InputBorder.none,
                                disabledBorder: InputBorder.none,
                                hintText: '',
                                hintStyle: TextStyle(fontSize: 10.sp),
                                contentPadding: EdgeInsets.only(left: 5.sp),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 70.sp,
                      child: ListView.builder(
                        scrollDirection: Axis.horizontal,
                        shrinkWrap: true,
                        itemCount: modelmotivasi
                            .where((element) =>
                                element.idmotivasi == widget.idmotiv)
                            .toList()
                            .first
                            .gambar!
                            .toList()
                            .length,
                        itemBuilder: (context, index) {
                          return Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Image.network(
                              ConfigUrl().base +
                                  ConfigUrl().imagepick +
                                  modelmotivasi
                                      .where((element) =>
                                          element.idmotivasi == widget.idmotiv)
                                      .first
                                      .gambar![index],
                              width: 70.sp,
                              fit: BoxFit.fill,
                            ),
                          );
                        },
                      ),
                    ),
                    if (files.toList().isNotEmpty) ...[
                      Container(
                        height: 20.h,
                        alignment: Alignment.centerLeft,
                        margin: const EdgeInsets.only(left: 30),
                        child: ListView.builder(
                          shrinkWrap: true,
                          scrollDirection: Axis.horizontal,
                          itemCount: files.length,
                          itemBuilder: (context, index) {
                            Logger().w(files.length);
                            return Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Image.file(
                                files[index].absolute,
                                width: 250,
                                fit: BoxFit.fill,
                              ),
                            );
                          },
                        ),
                      ),
                    ],
                    Container(
                      alignment: Alignment.centerLeft,
                      margin: EdgeInsets.only(
                        top: 20.sp,
                        left: 40.sp,
                        bottom: 10.sp,
                      ),
                      child: ElevatedButton(
                        onPressed: _pickFile,
                        child: const Text('Select File'),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 30.sp, bottom: 20.sp),
                      height: 30.sp,
                      width: 100.sp,
                      child: ElevatedButton(
                        style: ButtonStyle(
                          alignment: Alignment.center,
                          padding: MaterialStateProperty.all(EdgeInsets.zero),

                          backgroundColor: MaterialStateProperty.all<Color>(
                              Colors.black), // Ubah warna latar belakang
                          overlayColor: MaterialStateProperty.all<Color>(
                              Colors.red.shade300),
                          shadowColor:
                              MaterialStateProperty.all<Color>(Colors.black),

                          elevation: MaterialStateProperty.all<double>(
                              10), // Ubah elevasi
                          shape: MaterialStateProperty.all<OutlinedBorder>(
                            const StadiumBorder(), // Menggunakan StadiumBorder untuk bentuk oval
                          ),
                        ),
                        onPressed: () {
                          if (judul.text.isNotEmpty &&
                              isimotivasi.text.isNotEmpty &&
                              tags.text.isNotEmpty) {
                            MotivasiFunc().putMotivasi(
                                judul: judul.text,
                                isi: isimotivasi.text,
                                tags: tags.text,
                                datafile: files);
                            Future.delayed(const Duration(milliseconds: 100),
                                () {
                              NavigateApp()
                                  .pushReplace(context, const MyPostPage());
                            });
                          }
                        },
                        child: Text(
                          "Save",
                          style: GoogleFonts.rubik(
                            fontSize: 14.sp,
                            color: Colors.white,
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            )
          : const SizedBox(),
    );
  }
}
