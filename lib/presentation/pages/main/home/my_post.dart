import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:sizer/sizer.dart';
import 'package:vigenesia2024/data/auth.dart';
import 'package:vigenesia2024/data/configure_url.dart';
import 'package:vigenesia2024/data/motivasi.dart';
import 'package:vigenesia2024/domain/motivasi/motivasi.dart';
import 'package:vigenesia2024/domain/user/user.dart';
import 'package:vigenesia2024/helper/navigate.dart';
import 'package:vigenesia2024/helper/shared_pref.dart';
import 'package:vigenesia2024/helper/snackbar.dart';
import 'package:vigenesia2024/presentation/pages/main/home/Edit_motivasi.dart';
import 'package:vigenesia2024/presentation/pages/main/home/post_motivasi.dart';
import 'package:vigenesia2024/presentation/widget/showleftshet.dart';

class MyPostPage extends StatefulWidget {
  const MyPostPage({super.key});

  @override
  State<MyPostPage> createState() => _MyPostPageState();
}

class _MyPostPageState extends State<MyPostPage> {
  final motivasifetch = MotivasiData();
  final imageUserData = Userdata();
  List<User> userModel = [];

  List<Motivasi> modelmotivasi = [];
  String getUserImagePath(String username, List<User> userList) {
    for (var user in userList) {
      if (user.nama == username) {
        return user.gambar;
      }
    }
    return ''; // Return empty string jika tidak ditemukan
  }

  String iduser = '';
  @override
  void initState() {
    motivasifetch.getDataUser();
    imageUserData.getDataAll();
    SharedPrefApp().prefLoadUser().then((value) {
      setState(() {
        iduser = value[1];
      });
    });
    Future.delayed(Duration.zero, () {
      motivasifetch.motivasiStream.listen((event) {
        setState(() {
          modelmotivasi = event;
        });
      });
      imageUserData.userStream.listen((event) {
        setState(() {
          userModel = event;
        });
      });
    });
    super.initState();
  }

  @override
  void dispose() {
    motivasifetch.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        leading: Row(
          children: [
            IconButton(
                onPressed: () {
                  SnackBarPopUp().showLeftSheet(context, const ShowLeftSheet());
                },
                icon: Icon(
                  Icons.menu,
                  size: 25.sp,
                ))
          ],
        ),
      ),
      body: ListView.builder(
        itemCount: modelmotivasi.length,
        shrinkWrap: true,
        padding: EdgeInsets.only(
          left: 10.sp,
          right: 10.sp,
          bottom: 10.sp,
          top: 10.sp,
        ),
        itemBuilder: (context, index) {
          return Container(
            margin: EdgeInsets.only(top: 5.sp, bottom: 5.sp),
            padding: EdgeInsets.only(bottom: 5.sp),
            decoration: const BoxDecoration(
              border: Border(
                bottom: BorderSide(color: Colors.black, width: 1),
                right: BorderSide(color: Colors.black, width: 1),
              ),
            ),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                getUserImagePath(modelmotivasi[index].namauser, userModel)
                        .isNotEmpty
                    ? Container(
                        height: 60.sp,
                        width: 60.sp,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(60.sp),
                          image: DecorationImage(
                            image: NetworkImage(
                              ConfigUrl().base +
                                  ConfigUrl().imagepick +
                                  getUserImagePath(
                                      modelmotivasi[index].namauser, userModel),
                            ),
                          ),
                        ),
                      )
                    : Container(
                        height: 60.sp,
                        width: 60.sp,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(60.sp),
                          color: Colors.grey,
                        ),
                      ),
                SizedBox(
                  width: 10.sp,
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      width: 200.sp,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          IconButton(
                              onPressed: () {
                                NavigateApp().pushReplace(context, EditMotivasi(idmotiv: modelmotivasi[index].idmotivasi));
                              },
                              icon: Icon(
                                Icons.settings,
                                size: 20.sp,
                                color: Colors.black,
                              )),
                         
                        ],
                      ),
                    ),
                    Text(
                      modelmotivasi[index].namauser.toUpperCase(),
                      style: GoogleFonts.rubik(
                        fontSize: 12.sp,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    Text(
                      modelmotivasi[index].judul.toUpperCase(),
                      style: GoogleFonts.rubik(
                        fontSize: 11.sp,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    Container(
                      width: 200.sp,
                      padding: EdgeInsets.only(right: 3.sp),
                      child: Text(
                        modelmotivasi[index].isimotivasi,
                        style: GoogleFonts.rubik(
                          fontSize: 10.sp,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ),
                    modelmotivasi[index].gambar!.toList().isNotEmpty
                        ? SizedBox(
                            height: 70.sp,
                            child: ListView.builder(
                              scrollDirection: Axis.horizontal,
                              shrinkWrap: true,
                              itemCount:
                                  modelmotivasi[index].gambar?.toList().length,
                              itemBuilder: (context, indexG) {
                                return Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Image.network(
                                    ConfigUrl().base +
                                        ConfigUrl().imagepick +
                                        modelmotivasi[index].gambar![indexG],
                                    width: 70.sp,
                                    fit: BoxFit.fill,
                                  ),
                                );
                              },
                            ),
                          )
                        : const SizedBox(),
                    SizedBox(
                      width: 200.sp,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          IconButton(
                              onPressed: () {
                                MotivasiFunc().likeMotivasi(
                                    modelmotivasi[index].idmotivasi, iduser);
                              },
                              icon: Icon(
                                Icons.favorite,
                                size: 20.sp,
                                color: Colors.red.shade900,
                              )),
                          Text(
                            modelmotivasi[index].likemotivasi.toString(),
                            style: GoogleFonts.rubik(
                              fontSize: 14.sp,
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ],
            ),
          );
        },
      ),
      floatingActionButton: ElevatedButton(
          onPressed: () {
            NavigateApp().pushReplace(context, const PostMotivas());
          },
          child: Icon(
            Icons.add_task,
            size: 25.sp,
          )),
    );
  }
}
