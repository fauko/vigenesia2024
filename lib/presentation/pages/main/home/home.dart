import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:logger/logger.dart';
import 'package:sizer/sizer.dart';
import 'package:vigenesia2024/data/auth.dart';
import 'package:vigenesia2024/data/configure_url.dart';
import 'package:vigenesia2024/data/motivasi.dart';
import 'package:vigenesia2024/domain/motivasi/motivasi.dart';
import 'package:vigenesia2024/domain/user/user.dart';
import 'package:vigenesia2024/helper/shared_pref.dart';
import 'package:vigenesia2024/helper/snackbar.dart';
import 'package:vigenesia2024/presentation/widget/showleftshet.dart';

class HomeApp extends StatefulWidget {
  const HomeApp({super.key});

  @override
  State<HomeApp> createState() => _HomeAppState();
}

class _HomeAppState extends State<HomeApp> {
  TextEditingController search = TextEditingController();
  final getMotivasiData = MotivasiData();
  final imageUserData = Userdata();
  List<Motivasi> motivasiModel = [];
  List<User> userModel = [];
  int selectidx = 0;
  List<Widget> screens = [];
  String iduser = '';
  String getUserImagePath(String username, List<User> userList) {
    for (var user in userList) {
      if (user.nama == username) {
        return user.gambar;
      }
    }
    return ''; // Return empty string jika tidak ditemukan
  }

  @override
  void initState() {
    SharedPrefApp().prefLoadUser().then((value) {
      setState(() {
        iduser = value[1];
      });
    });
    getMotivasiData.getDataAll();
    imageUserData.getDataAll();
    Future.delayed(const Duration(milliseconds: 50), () {
      getMotivasiData.motivasiStream.listen((event) {
        setState(() {
          motivasiModel = event;
        });
      });
      imageUserData.userStream.listen((event) {
        setState(() {
          userModel = event;
        });
      });
    });

    super.initState();
  }

  @override
  void dispose() {
    getMotivasiData.dispose();
    imageUserData.dispose();
    super.dispose();
  }

  @override
  void setState(fn) {
    if (mounted) {
      super.setState(fn);
    }
  }

  @override
  Widget build(BuildContext context) {
    // Logger().w(motivasiModel.length);
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        leading: Row(
          children: [
            IconButton(
                onPressed: () {
                  SnackBarPopUp().showLeftSheet(context, const ShowLeftSheet());
                },
                icon: Icon(
                  Icons.menu,
                  size: 25.sp,
                ))
          ],
        ),
      ),
      body: selectidx == 0
          ? ListView.builder(
              itemCount: motivasiModel.length,
              shrinkWrap: true,
              padding: EdgeInsets.only(
                left: 10.sp,
                right: 10.sp,
                bottom: 10.sp,
                top: 10.sp,
              ),
              itemBuilder: (context, index) {
                return Container(
                  margin: EdgeInsets.only(top: 5.sp, bottom: 5.sp),
                  padding: EdgeInsets.only(bottom: 5.sp),
                  decoration: const BoxDecoration(
                    border: Border(
                      bottom: BorderSide(color: Colors.black, width: 1),
                      right: BorderSide(color: Colors.black, width: 1),
                    ),
                  ),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      getUserImagePath(motivasiModel[index].namauser, userModel)
                              .isNotEmpty
                          ? Container(
                              height: 60.sp,
                              width: 60.sp,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(60.sp),
                                image: DecorationImage(
                                  image: NetworkImage(
                                    ConfigUrl().base +
                                        ConfigUrl().imagepick +
                                        getUserImagePath(
                                            motivasiModel[index].namauser,
                                            userModel),
                                  ),
                                ),
                              ),
                            )
                          : Container(
                              height: 60.sp,
                              width: 60.sp,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(60.sp),
                                color: Colors.grey,
                              ),
                            ),
                      SizedBox(
                        width: 10.sp,
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            motivasiModel[index].namauser.toUpperCase(),
                            style: GoogleFonts.rubik(
                              fontSize: 12.sp,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          Text(
                            motivasiModel[index].judul.toUpperCase(),
                            style: GoogleFonts.rubik(
                              fontSize: 11.sp,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          Container(
                            width: 200.sp,
                            padding: EdgeInsets.only(right: 3.sp),
                            child: Text(
                              motivasiModel[index].isimotivasi,
                              style: GoogleFonts.rubik(
                                fontSize: 10.sp,
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                          ),
                          motivasiModel[index].gambar!.toList().isNotEmpty
                        ? SizedBox(
                            height: 70.sp,
                            child: ListView.builder(
                              scrollDirection: Axis.horizontal,
                              shrinkWrap: true,
                              itemCount:
                                  motivasiModel[index].gambar?.toList().length,
                              itemBuilder: (context, indexG) {
                                return Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Image.network(
                                    ConfigUrl().base +
                                        ConfigUrl().imagepick +
                                        motivasiModel[index].gambar![indexG],
                                    width: 70.sp,
                                    fit: BoxFit.fill,
                                  ),
                                );
                              },
                            ),
                          )
                        : const SizedBox(),
                          SizedBox(
                            width: 200.sp,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                IconButton(
                                    onPressed: () {
                                      MotivasiFunc().likeMotivasi(
                                          motivasiModel[index].idmotivasi,
                                          iduser);
                                    },
                                    icon: Icon(
                                      Icons.favorite,
                                      size: 20.sp,
                                      color: Colors.red.shade900,
                                    )),
                                Text(
                                  motivasiModel[index].likemotivasi.toString(),
                                  style: GoogleFonts.rubik(
                                    fontSize: 14.sp,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ],
                  ),
                );
              },
            )
          :  SizedBox(
                  width: MediaQuery.sizeOf(context).width,
                  child: SingleChildScrollView(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Form(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Container(
                                width: 260.sp,
                                decoration: BoxDecoration(
                                  boxShadow: const [
                                    BoxShadow(
                                        color: Colors.black26,
                                        offset: Offset(0, 1),
                                        blurRadius: 6.0)
                                  ],
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(10.0),
                                ),
                                child: Row(
                                  children: [
                                    Container(
                                      height: 40.sp,
                                      width: 200.sp,
                                      margin: const EdgeInsets.only(top: 10),
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 20),
                                      child: TextFormField(
                                        controller: search,
                                        decoration: InputDecoration(
                                          //   prefixIcon: prefixIcon,
                                          border: InputBorder.none,
                                          focusedBorder: InputBorder.none,
                                          enabledBorder: InputBorder.none,
                                          errorBorder: InputBorder.none,
                                          disabledBorder: InputBorder.none,
                                          hintText: '',
                                          hintStyle: TextStyle(fontSize: 10.sp),
                                          contentPadding:
                                              EdgeInsets.only(left: 5.sp),
                                        ),
                                      ),
                                    ),
                                    const Spacer(),
                                    Container(
                                      margin: EdgeInsets.only(right: 10.sp),
                                      child: Icon(
                                        Icons.search,
                                        size: 25.sp,
                                      ),
                                    )
                                  ],
                                ),
                              ),
                              SizedBox(
                                height: 20.sp,
                              ),
                              ListView.builder(
                                itemCount: motivasiModel
                                    .where((element) =>
                                        element.judul == search.text)
                                    .toList()
                                    .length,
                                shrinkWrap: true,
                                padding: EdgeInsets.only(
                                  left: 10.sp,
                                  right: 10.sp,
                                  bottom: 10.sp,
                                  top: 10.sp,
                                ),
                                itemBuilder: (context, index) {
                                  return Container(
                                    margin: EdgeInsets.only(
                                        top: 5.sp, bottom: 5.sp),
                                    padding: EdgeInsets.only(bottom: 5.sp),
                                    decoration: const BoxDecoration(
                                      border: Border(
                                        bottom: BorderSide(
                                            color: Colors.black, width: 1),
                                        right: BorderSide(
                                            color: Colors.black, width: 1),
                                      ),
                                    ),
                                    child: Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        getUserImagePath(
                                                    motivasiModel
                                                        .where((element) =>
                                                            element.judul
                                                                .contains(search
                                                                    .text))
                                                        .toList()[index]
                                                        .namauser,
                                                    userModel)
                                                .isNotEmpty
                                            ? Container(
                                                height: 60.sp,
                                                width: 60.sp,
                                                decoration: BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          60.sp),
                                                  image: DecorationImage(
                                                    image: NetworkImage(
                                                      ConfigUrl().base +
                                                          ConfigUrl()
                                                              .imagepick +
                                                          getUserImagePath(
                                                              motivasiModel
                                                                  .where((element) => element
                                                                      .judul
                                                                      .contains(
                                                                          search
                                                                              .text))
                                                                  .toList()[
                                                                      index]
                                                                  .namauser,
                                                              userModel),
                                                    ),
                                                  ),
                                                ),
                                              )
                                            : Container(
                                                height: 60.sp,
                                                width: 60.sp,
                                                decoration: BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          60.sp),
                                                  color: Colors.grey,
                                                ),
                                              ),
                                        SizedBox(
                                          width: 10.sp,
                                        ),
                                        Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              motivasiModel
                                                  .where((element) => element
                                                      .judul
                                                      .contains(search.text))
                                                  .toList()[index]
                                                  .namauser
                                                  .toUpperCase(),
                                              style: GoogleFonts.rubik(
                                                fontSize: 12.sp,
                                                fontWeight: FontWeight.bold,
                                              ),
                                            ),
                                            Text(
                                              motivasiModel
                                                  .where((element) => element
                                                      .judul
                                                      .contains(search.text))
                                                  .toList()[index]
                                                  .judul
                                                  .toUpperCase(),
                                              style: GoogleFonts.rubik(
                                                fontSize: 11.sp,
                                                fontWeight: FontWeight.bold,
                                              ),
                                            ),
                                            Container(
                                              width: 200.sp,
                                              padding:
                                                  EdgeInsets.only(right: 3.sp),
                                              child: Text(
                                                motivasiModel
                                                    .where((element) => element
                                                        .judul
                                                        .contains(search.text))
                                                    .toList()[index]
                                                    .isimotivasi,
                                                style: GoogleFonts.rubik(
                                                  fontSize: 10.sp,
                                                  fontWeight: FontWeight.w500,
                                                ),
                                              ),
                                            ),
                                            SizedBox(
                                              width: 200.sp,
                                              child: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.end,
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.center,
                                                children: [
                                                  IconButton(
                                                      onPressed: () {
                                                        MotivasiFunc().likeMotivasi(
                                                            motivasiModel
                                                                .where((element) =>
                                                                    element
                                                                        .judul
                                                                        .contains(
                                                                            search.text))
                                                                .toList()[index]
                                                                .idmotivasi,
                                                            iduser);
                                                      },
                                                      icon: Icon(
                                                        Icons.favorite,
                                                        size: 20.sp,
                                                        color:
                                                            Colors.red.shade900,
                                                      )),
                                                  Text(
                                                    motivasiModel
                                                        .where((element) =>
                                                            element.judul
                                                                .contains(search
                                                                    .text))
                                                        .toList()[index]
                                                        .likemotivasi
                                                        .toString(),
                                                    style: GoogleFonts.rubik(
                                                      fontSize: 14.sp,
                                                      fontWeight:
                                                          FontWeight.w500,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            )
                                          ],
                                        ),
                                      ],
                                    ),
                                  );
                                },
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                )
             ,
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: Colors.white,
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            label: "HOME",
            icon: Icon(
              Icons.home,
              size: 25.sp,
            ),
          ),
          BottomNavigationBarItem(
              icon: Icon(
                Icons.search,
                size: 25.sp,
              ),
              label: "SEARCH"),
        
        ],
        currentIndex: selectidx,
        selectedItemColor: Colors.black,
        onTap: (index) {
          setState(() {
            selectidx = index;
          });
        },
      ),
    );
  }
}
