import 'package:flutter/material.dart';
import 'package:geocoding/geocoding.dart';
import 'package:geolocator/geolocator.dart';
import 'package:logger/logger.dart';

import 'package:sizer/sizer.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:vigenesia2024/data/auth.dart';
import 'package:vigenesia2024/data/motivasi.dart';
import 'package:vigenesia2024/helper/navigate.dart';
import 'package:vigenesia2024/helper/snackbar.dart';
import 'package:vigenesia2024/presentation/pages/auth/sign_up.dart';

class SignInPage extends StatefulWidget {
  const SignInPage({super.key});

  @override
  State<SignInPage> createState() => _SignInPageState();
}

class _SignInPageState extends State<SignInPage> {
  TextEditingController email = TextEditingController();
  TextEditingController password = TextEditingController();
  double lat = 0.0;
  double long = 0.0;
  var area = '';
  Future<void> getCurrentLocation() async {
    LocationPermission permission;
    permission = await Geolocator.requestPermission();
    try {
      if (permission == LocationPermission.deniedForever) {
        Position position = await Geolocator.getCurrentPosition(
            forceAndroidLocationManager: false,
            desiredAccuracy: LocationAccuracy.best);
        List<Placemark> placemarks = await placemarkFromCoordinates(
            position.latitude, position.longitude);

        Placemark place = placemarks[0];
        // Logger().i(position.latitude);
        setState(() {
          lat = position.latitude;
          long = position.longitude;
          // lokasi = position;
          area =
              "${place.administrativeArea} ${place.subAdministrativeArea} ${place.subLocality} ${place.street} ${place.postalCode}";
        });
        Logger().i(area);
      }
      // else {
      //   Position position = await Geolocator.getCurrentPosition(
      //       forceAndroidLocationManager: false,
      //       desiredAccuracy: LocationAccuracy.best);
      //   List<Placemark> placemarks = await placemarkFromCoordinates(
      //       position.latitude, position.longitude);

      //   Placemark place = placemarks[0];

      //   setState(() {
      //     latitude = position.latitude;
      //     longitide = position.longitude;
      //     area =
      //         "${place.administrativeArea} ${place.subAdministrativeArea} ${place.subLocality} ${place.street} ${place.postalCode}";
      //   });
      // }
    } catch (e) {
      Logger().e(e);
    }
  }

  @override
  void initState() {
    MotivasiData().dispose();
    Userdata().dispose();
    super.initState();
  }

  @override
  void setState(fn) {
    if (mounted) {
      super.setState(fn);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        height: MediaQuery.sizeOf(context).height,
        width: MediaQuery.sizeOf(context).width,
        padding: EdgeInsets.zero,
        child: SingleChildScrollView(
          child: Column(
            children: [
              SizedBox(
                height: 70.sp,
              ),
              Text(
                "SigIn",
                style: GoogleFonts.rubik(
                  fontSize: 18.sp,
                  fontWeight: FontWeight.bold,
                ),
              ),
              SizedBox(
                height: 100.sp,
              ),
              Form(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Email:",
                      style: GoogleFonts.rubik(
                        fontSize: 12.sp,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    Container(
                      height: 40.sp,
                      width: 200.sp,
                      margin: const EdgeInsets.only(top: 10),
                      padding: const EdgeInsets.symmetric(horizontal: 20),
                      decoration: BoxDecoration(
                        boxShadow: const [
                          BoxShadow(
                              color: Colors.black26,
                              offset: Offset(0, 1),
                              blurRadius: 6.0)
                        ],
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                      child: TextFormField(
                        controller: email,
                        decoration: InputDecoration(
                          //   prefixIcon: prefixIcon,
                          border: InputBorder.none,
                          focusedBorder: InputBorder.none,
                          enabledBorder: InputBorder.none,
                          errorBorder: InputBorder.none,
                          disabledBorder: InputBorder.none,
                          hintText: "",
                          hintStyle: TextStyle(fontSize: 10.sp),
                          contentPadding: EdgeInsets.only(left: 5.sp),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 20.sp,
                    ),
                    Text(
                      "Password:",
                      style: GoogleFonts.rubik(
                        fontSize: 12.sp,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    Container(
                      height: 40.sp,
                      width: 200.sp,
                      margin: const EdgeInsets.only(top: 10),
                      padding: const EdgeInsets.symmetric(horizontal: 20),
                      decoration: BoxDecoration(
                        boxShadow: const [
                          BoxShadow(
                              color: Colors.black26,
                              offset: Offset(0, 1),
                              blurRadius: 6.0)
                        ],
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                      child: TextFormField(
                        controller: password,
                        decoration: InputDecoration(
                          //   prefixIcon: prefixIcon,
                          border: InputBorder.none,
                          focusedBorder: InputBorder.none,
                          enabledBorder: InputBorder.none,
                          errorBorder: InputBorder.none,
                          disabledBorder: InputBorder.none,
                          hintText: "",
                          hintStyle: TextStyle(fontSize: 10.sp),
                          contentPadding: EdgeInsets.only(left: 5.sp),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 90.sp, bottom: 20.sp),
                height: 30.sp,
                width: 100.sp,
                child: ElevatedButton(
                  style: ButtonStyle(
                    alignment: Alignment.center,
                    padding: MaterialStateProperty.all(EdgeInsets.zero),

                    backgroundColor: MaterialStateProperty.all<Color>(
                        Colors.black), // Ubah warna latar belakang
                    overlayColor:
                        MaterialStateProperty.all<Color>(Colors.red.shade300),
                    shadowColor: MaterialStateProperty.all<Color>(Colors.black),

                    elevation:
                        MaterialStateProperty.all<double>(10), // Ubah elevasi
                    shape: MaterialStateProperty.all<OutlinedBorder>(
                      const StadiumBorder(), // Menggunakan StadiumBorder untuk bentuk oval
                    ),
                  ),
                  onPressed: ()  {
                    if (email.text.isNotEmpty && password.text.isNotEmpty) {
                      Auth().login(
                        context: context,
                        email: email.text,
                        password: password.text,
                        alamat: area,
                      );
                    }
                  },
                  child: Text(
                    "Sign In",
                    style: GoogleFonts.rubik(
                      fontSize: 14.sp,
                      color: Colors.white,
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    "don't have an account yet ? ",
                    style: GoogleFonts.rubik(
                      fontSize: 11.sp,
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      NavigateApp().pushPage(context, const SignUpPage());
                    },
                    child: Text(
                      "SignUp",
                      style: GoogleFonts.rubik(
                        fontSize: 11.sp,
                        fontWeight: FontWeight.bold,
                        color: Colors.blue,
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
