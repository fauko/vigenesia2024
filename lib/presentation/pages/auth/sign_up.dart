import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:sizer/sizer.dart';
import 'package:vigenesia2024/data/auth.dart';
import 'package:vigenesia2024/helper/navigate.dart';
import 'package:vigenesia2024/presentation/pages/auth/sign_in.dart';

class SignUpPage extends StatefulWidget {
  const SignUpPage({super.key});

  @override
  State<SignUpPage> createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {
  TextEditingController nama = TextEditingController();
  TextEditingController profesi = TextEditingController();
  TextEditingController email = TextEditingController();
  TextEditingController password = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        height: MediaQuery.sizeOf(context).height,
        width: MediaQuery.sizeOf(context).width,
        padding: EdgeInsets.zero,
        child: SingleChildScrollView(
          child: Column(
            children: [
              SizedBox(
                height: 70.sp,
              ),
              Text(
                "Sign Up",
                style: GoogleFonts.rubik(
                  fontSize: 18.sp,
                  fontWeight: FontWeight.bold,
                ),
              ),
              SizedBox(
                height: 50.sp,
              ),
              Form(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Nama:",
                      style: GoogleFonts.rubik(
                        fontSize: 12.sp,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    Container(
                      height: 40.sp,
                      width: 200.sp,
                      margin: const EdgeInsets.only(top: 10),
                      padding: const EdgeInsets.symmetric(horizontal: 20),
                      decoration: BoxDecoration(
                        boxShadow: const [
                          BoxShadow(
                              color: Colors.black26,
                              offset: Offset(0, 1),
                              blurRadius: 6.0)
                        ],
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                      child: TextFormField(
                        controller: nama,
                        decoration: InputDecoration(
                          //   prefixIcon: prefixIcon,
                          border: InputBorder.none,
                          focusedBorder: InputBorder.none,
                          enabledBorder: InputBorder.none,
                          errorBorder: InputBorder.none,
                          disabledBorder: InputBorder.none,
                          hintText: "",
                          hintStyle: TextStyle(fontSize: 10.sp),
                          contentPadding: EdgeInsets.only(left: 5.sp),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 20.sp,
                    ),
                    Text(
                      "Profesi:",
                      style: GoogleFonts.rubik(
                        fontSize: 12.sp,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    Container(
                      height: 40.sp,
                      width: 200.sp,
                      margin: const EdgeInsets.only(top: 10),
                      padding: const EdgeInsets.symmetric(horizontal: 20),
                      decoration: BoxDecoration(
                        boxShadow: const [
                          BoxShadow(
                              color: Colors.black26,
                              offset: Offset(0, 1),
                              blurRadius: 6.0)
                        ],
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                      child: TextFormField(
                        controller: profesi,
                        decoration: InputDecoration(
                          //   prefixIcon: prefixIcon,
                          border: InputBorder.none,
                          focusedBorder: InputBorder.none,
                          enabledBorder: InputBorder.none,
                          errorBorder: InputBorder.none,
                          disabledBorder: InputBorder.none,
                          hintText: "",
                          hintStyle: TextStyle(fontSize: 10.sp),
                          contentPadding: EdgeInsets.only(left: 5.sp),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 20.sp,
                    ),
                    Text(
                      "Email:",
                      style: GoogleFonts.rubik(
                        fontSize: 12.sp,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    Container(
                      height: 40.sp,
                      width: 200.sp,
                      margin: const EdgeInsets.only(top: 10),
                      padding: const EdgeInsets.symmetric(horizontal: 20),
                      decoration: BoxDecoration(
                        boxShadow: const [
                          BoxShadow(
                              color: Colors.black26,
                              offset: Offset(0, 1),
                              blurRadius: 6.0)
                        ],
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                      child: TextFormField(
                        controller: email,
                        decoration: InputDecoration(
                          //   prefixIcon: prefixIcon,
                          border: InputBorder.none,
                          focusedBorder: InputBorder.none,
                          enabledBorder: InputBorder.none,
                          errorBorder: InputBorder.none,
                          disabledBorder: InputBorder.none,
                          hintText: "",
                          hintStyle: TextStyle(fontSize: 10.sp),
                          contentPadding: EdgeInsets.only(left: 5.sp),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 20.sp,
                    ),
                    Text(
                      "Password:",
                      style: GoogleFonts.rubik(
                        fontSize: 12.sp,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    Container(
                      height: 40.sp,
                      width: 200.sp,
                      margin: const EdgeInsets.only(top: 10),
                      padding: const EdgeInsets.symmetric(horizontal: 20),
                      decoration: BoxDecoration(
                        boxShadow: const [
                          BoxShadow(
                              color: Colors.black26,
                              offset: Offset(0, 1),
                              blurRadius: 6.0)
                        ],
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                      child: TextFormField(
                        controller: password,
                        decoration: InputDecoration(
                          //   prefixIcon: prefixIcon,
                          border: InputBorder.none,
                          focusedBorder: InputBorder.none,
                          enabledBorder: InputBorder.none,
                          errorBorder: InputBorder.none,
                          disabledBorder: InputBorder.none,
                          hintText: "",
                          hintStyle: TextStyle(fontSize: 10.sp),
                          contentPadding: EdgeInsets.only(left: 5.sp),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 50.sp, bottom: 20.sp),
                height: 30.sp,
                width: 100.sp,
                child: ElevatedButton(
                  style: ButtonStyle(
                    alignment: Alignment.center,
                    padding: MaterialStateProperty.all(EdgeInsets.zero),

                    backgroundColor: MaterialStateProperty.all<Color>(
                        Colors.black), // Ubah warna latar belakang
                    overlayColor:
                        MaterialStateProperty.all<Color>(Colors.red.shade300),
                    shadowColor: MaterialStateProperty.all<Color>(Colors.black),

                    elevation:
                        MaterialStateProperty.all<double>(10), // Ubah elevasi
                    shape: MaterialStateProperty.all<OutlinedBorder>(
                      const StadiumBorder(), // Menggunakan StadiumBorder untuk bentuk oval
                    ),
                  ),
                  onPressed: () {
                    if (nama.text.isNotEmpty && profesi.text.isNotEmpty && email.text.isNotEmpty && password.text.isNotEmpty) {
                      Auth().daftar( context: context,nama: nama.text , profesi: profesi.text , email:email.text , password:  password.text);
                    }
                  },
                  child: Text(
                    "Sign Up",
                    style: GoogleFonts.rubik(
                      fontSize: 14.sp,
                      color: Colors.white,
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    "have an account yet ? ",
                    style: GoogleFonts.rubik(
                      fontSize: 11.sp,
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      NavigateApp().pushPage(context, const SignInPage());
                    },
                    child: Text(
                      "SignIn",
                      style: GoogleFonts.rubik(
                        fontSize: 11.sp,
                        fontWeight: FontWeight.bold,
                        color: Colors.blue,
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
