import 'dart:io';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:logger/logger.dart';
import 'package:sizer/sizer.dart';
import 'package:vigenesia2024/data/auth.dart';
import 'package:vigenesia2024/data/configure_url.dart';
import 'package:vigenesia2024/helper/shared_pref.dart';
import 'package:vigenesia2024/helper/snackbar.dart';
import 'package:vigenesia2024/presentation/widget/showleftshet.dart';
import 'package:vigenesia2024/domain/user/user.dart';

class ProfilePages extends StatefulWidget {
  const ProfilePages({super.key});

  @override
  State<ProfilePages> createState() => _ProfilePagesState();
}

class _ProfilePagesState extends State<ProfilePages> {
  final userfetch = Userdata();
  List<User> userModel = [];
  String iduser = '';
  File files = File('');
  Future<void> _pickFile() async {
    final result = await FilePicker.platform.pickFiles(
      allowMultiple: false,
      type: FileType.custom,
      allowedExtensions: ['png', 'jpg', 'jpeg'],
      // allowedExtensions: ['xlsx', 'xls', 'pdf' , ],
    );

    if (result != null) {
      setState(() {
        Logger().w(files);
        files = File(result.files.single.path!);
      });
    }
  }

  @override
  void initState() {
    SharedPrefApp().prefLoadUser().then((value) {
      setState(() {
        iduser = value[1];
      });
    });
    userfetch.getDataAll();
    Future.delayed(Duration.zero, () {
      userfetch.userStream.listen((event) {
        setState(() {
          userModel =
              event.where((element) => element.iduser == iduser).toList();
        });
      });
    });
    super.initState();
  }

  @override
  void dispose() {
    userfetch.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    TextEditingController nama =
        TextEditingController(text:  userModel.isNotEmpty
          ?userModel.first.nama : '');
    TextEditingController profesi =
        TextEditingController(text:  userModel.isNotEmpty
          ? userModel.first.profesi : '');

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        leading: Row(
          children: [
            IconButton(
                onPressed: () {
                  SnackBarPopUp().showLeftSheet(context, const ShowLeftSheet());
                },
                icon: Icon(
                  Icons.menu,
                  size: 25.sp,
                ))
          ],
        ),
      ),
      body: userModel.isNotEmpty
          ? SizedBox(
              width: MediaQuery.sizeOf(context).width,
              height: MediaQuery.sizeOf(context).height,
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    userModel.first.gambar.isNotEmpty
                        ? GestureDetector(
                            onTap: () {
                              _pickFile();
                            },
                            child: files.path.isEmpty
                                ? Container(
                                    height: 70.sp,
                                    width: 70.sp,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(70.sp),
                                      image: DecorationImage(
                                        image: NetworkImage(ConfigUrl().base +
                                            ConfigUrl().imagepick +
                                            userModel.first.gambar),
                                      ),
                                    ),
                                  )
                                : Container(
                                    height: 70.sp,
                                    width: 70.sp,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(70.sp),
                                      image: DecorationImage(
                                        image: FileImage(files, scale: 1),
                                      ),
                                    ),
                                  ),
                          )
                        : GestureDetector(
                            onTap: () {
                              _pickFile();
                            },
                            child: files.path.isEmpty
                                ? Container(
                                    height: 70.sp,
                                    width: 70.sp,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(70.sp),
                                      color: Colors.grey,
                                    ),
                                  )
                                : Container(
                                    height: 70.sp,
                                    width: 70.sp,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(70.sp),
                                      image: DecorationImage(
                                        image: FileImage(files, scale: 1),
                                      ),
                                    ),
                                  ),
                          ),
                    SizedBox(
                      height: 20.sp,
                    ),
                    Form(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Nama:",
                            style: GoogleFonts.rubik(
                              fontSize: 12.sp,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          Container(
                            height: 40.sp,
                            width: 200.sp,
                            margin: const EdgeInsets.only(top: 10),
                            padding: const EdgeInsets.symmetric(horizontal: 20),
                            decoration: BoxDecoration(
                              boxShadow: const [
                                BoxShadow(
                                    color: Colors.black26,
                                    offset: Offset(0, 1),
                                    blurRadius: 6.0)
                              ],
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(10.0),
                            ),
                            child: TextFormField(
                              controller: nama,
                              readOnly: true,
                              decoration: InputDecoration(
                                //   prefixIcon: prefixIcon,
                                border: InputBorder.none,
                                focusedBorder: InputBorder.none,
                                enabledBorder: InputBorder.none,
                                errorBorder: InputBorder.none,
                                disabledBorder: InputBorder.none,
                                hintText: '',
                                hintStyle: TextStyle(fontSize: 10.sp),
                                contentPadding: EdgeInsets.only(left: 5.sp),
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 20.sp,
                          ),
                          Text(
                            "Profesi:",
                            style: GoogleFonts.rubik(
                              fontSize: 12.sp,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          Container(
                            height: 40.sp,
                            width: 200.sp,
                            margin: const EdgeInsets.only(top: 10),
                            padding: const EdgeInsets.symmetric(horizontal: 20),
                            decoration: BoxDecoration(
                              boxShadow: const [
                                BoxShadow(
                                    color: Colors.black26,
                                    offset: Offset(0, 1),
                                    blurRadius: 6.0)
                              ],
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(10.0),
                            ),
                            child: TextFormField(
                              controller: profesi,
                              decoration: InputDecoration(
                                //   prefixIcon: prefixIcon,
                                border: InputBorder.none,
                                focusedBorder: InputBorder.none,
                                enabledBorder: InputBorder.none,
                                errorBorder: InputBorder.none,
                                disabledBorder: InputBorder.none,
                                hintText: '',
                                hintStyle: TextStyle(fontSize: 10.sp),
                                contentPadding: EdgeInsets.only(left: 5.sp),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                   
                    Container(
                      margin: EdgeInsets.only(top: 90.sp, bottom: 20.sp),
                      height: 30.sp,
                      width: 150.sp,
                      child: ElevatedButton(
                        style: ButtonStyle(
                          alignment: Alignment.center,
                          padding: MaterialStateProperty.all(EdgeInsets.zero),
                
                          backgroundColor: MaterialStateProperty.all<Color>(
                              Colors.black), // Ubah warna latar belakang
                          overlayColor: MaterialStateProperty.all<Color>(
                              Colors.red.shade300),
                          shadowColor:
                              MaterialStateProperty.all<Color>(Colors.black),
                
                          elevation: MaterialStateProperty.all<double>(
                              10), // Ubah elevasi
                          shape: MaterialStateProperty.all<OutlinedBorder>(
                            const StadiumBorder(), // Menggunakan StadiumBorder untuk bentuk oval
                          ),
                        ),
                        onPressed: () {
                          if (nama.text.isNotEmpty && profesi.text.isNotEmpty) {
                            userfetch.editProfile(nama.text, profesi.text, files , userModel.first.password);
                          }
                        },
                        child: Text(
                          "Update Profile",
                          style: GoogleFonts.rubik(
                            fontSize: 14.sp,
                            color: Colors.white,
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            )
          : Container(),
    );
  }
}
