import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:sizer/sizer.dart';
import 'package:vigenesia2024/data/auth.dart';
import 'package:vigenesia2024/data/configure_url.dart';
import 'package:vigenesia2024/domain/user/user.dart';
import 'package:vigenesia2024/helper/navigate.dart';
import 'package:vigenesia2024/helper/shared_pref.dart';
import 'package:vigenesia2024/presentation/pages/main/home/home.dart';
import 'package:vigenesia2024/presentation/pages/main/home/my_post.dart';
import 'package:vigenesia2024/presentation/pages/main/home/post_motivasi.dart';
import 'package:vigenesia2024/presentation/pages/user/profile.dart';

class ShowLeftSheet extends StatefulWidget {
  const ShowLeftSheet({super.key});

  @override
  State<ShowLeftSheet> createState() => _ShowLeftSheetState();
}

class _ShowLeftSheetState extends State<ShowLeftSheet> {
  final userfetch = Userdata();
  List<User> userModel = [];
  String iduser = '';
  String id = '';

  @override
  void initState() {
    userfetch.getDataAll();
    SharedPrefApp().prefLoadUser().then((value) {
      setState(() {
      id = value[1];
        iduser = value[2];
      });
    });
    Future.delayed(Duration.zero, () {
      userfetch.userStream.listen((event) {
        setState(() {
          userModel = event;
        });
      });
    });

    super.initState();
  }

  @override
  void dispose() {
    userfetch.dispose();
    super.dispose();
  }

  @override
  void setState(fn) {
    if (mounted) {
      super.setState(fn);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.sizeOf(context).height,
      width: 160.sp,
      margin: EdgeInsets.only(top: 30.sp),
      padding: EdgeInsets.all(20.sp),
      decoration: const BoxDecoration(
        color: Colors.white,
        border: Border(
          right: BorderSide(color: Colors.black, width: 1),
        ),
      ),
      child:  userModel.isNotEmpty ? Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
      userModel.where((element) => element.iduser == id).first.gambar.isEmpty
              ? Container(
                  height: 70.sp,
                  width: 70.sp,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(70.sp),
                    color: Colors.grey,
                  ),
                )
              : Container(
                  height: 70.sp,
                  width: 70.sp,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(70.sp),
                      image: DecorationImage(
                          image: NetworkImage(ConfigUrl().base +
                              ConfigUrl().imagepick +
                              userModel.where((element) => element.iduser == id).first.gambar))),
                ),
          SizedBox(
            height: 20.sp,
          ),
          GestureDetector(
            onTap: () {
              NavigateApp().pushReplace(context, const ProfilePages());
            },
            child: Text(
              "Setting Account",
              style: GoogleFonts.rubik(
                fontSize: 11.sp,
                color: Colors.black,
                fontWeight: FontWeight.w400,
              ),
            ),
          ),
          SizedBox(
            height: 10.sp,
          ),
          Text(
            iduser,
            style: GoogleFonts.rubik(
              fontSize: 12.sp,
              color: Colors.black,
              fontWeight: FontWeight.bold,
            ),
          ),
          SizedBox(
            height: 15.sp,
          ),
          GestureDetector(
            onTap: () {
              NavigateApp().pushReplace(context, const HomeApp());
            },
            child: Text(
              "Home",
              style: GoogleFonts.rubik(
                fontSize: 12.sp,
                color: Colors.black,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          SizedBox(
            height: 15.sp,
          ),
          GestureDetector(
            onTap: () {
              NavigateApp().pushReplace(context, const MyPostPage());
            },
            child: Text(
              "MyPost",
              style: GoogleFonts.rubik(
                fontSize: 12.sp,
                color: Colors.black,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          const Spacer(),
          GestureDetector(
            onTap: () {},
            child: Text(
              "LogOut",
              style: GoogleFonts.rubik(
                fontSize: 12.sp,
                color: Colors.black,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
        ],
      ) : Container(),
    );
  }
}
