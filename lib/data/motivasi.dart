import 'dart:async';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:logger/logger.dart';
import 'package:vigenesia2024/data/configure_url.dart';
import 'package:vigenesia2024/domain/motivasi/motivasi.dart';
import 'package:vigenesia2024/helper/shared_pref.dart';

class MotivasiData {
  final dio = Dio();
  StreamController<List<Motivasi>> _motivasiStreamController =
      StreamController<List<Motivasi>>.broadcast();

  Stream<List<Motivasi>> get motivasiStream => _motivasiStreamController.stream;
  bool whilecontinue = true;
  void initStreamController() {
    _motivasiStreamController = StreamController<List<Motivasi>>.broadcast();
  }

  Future<void> dispose() async {
    whilecontinue = false;

    await _motivasiStreamController.close();
  }

  Future<void> getDataAll() async {
    var token = '';
    await SharedPrefApp().prefLoadUser().then((value) {
      token = value[0];
    });
    while (whilecontinue) {
      dio.options.headers['Authorization'] = 'Bearer $token';
      try {
        final response =
            await dio.get(ConfigUrl().base + ConfigUrl().getMotivasi);
        List<dynamic> data = response.data;
        List<Motivasi> motivasiData =
            data.map((e) => Motivasi.fromJson(e)).toList();
        _motivasiStreamController.add(motivasiData);
      } catch (e) {
        // Logger().e(e);
      }

      Future.delayed(const Duration(seconds: 5));
    }
  }

  Future<void> getDataUser() async {
    var token = '';
    var iduser = '';
    await SharedPrefApp().prefLoadUser().then((value) {
      token = value[0];
      iduser = value[1];
    });
    while (whilecontinue) {
      dio.options.headers['Authorization'] = 'Bearer $token';
      try {
        final response = await dio.get(
            "${ConfigUrl().base}${ConfigUrl().getMotivasi}?iduser=$iduser");
        List<dynamic> data = response.data;
        List<Motivasi> motivasiData =
            data.map((e) => Motivasi.fromJson(e)).toList();
        _motivasiStreamController.add(motivasiData);
      } catch (e) {
        // Logger().e(e);
      }

      Future.delayed(const Duration(seconds: 5));
    }
  }
}

class MotivasiFunc {
  final dio = Dio();
  Future<void> likeMotivasi(String idmotivasi, String iduser) async {
    var token = '';
    await SharedPrefApp().prefLoadUser().then((value) {
      token = value[0];
    });
    dio.options.headers['Authorization'] = 'Bearer $token';

    final FormData formdat =
        FormData.fromMap({"idmotivasi": idmotivasi, "iduser": iduser});
    try {
      final respon = await dio.post(
        ConfigUrl().base + ConfigUrl().likeMotivasi,
        data: formdat,
      );
      Logger().i(formdat.fields);
      Logger().w(respon.data);
    } catch (e) {
      final respon = await dio.delete(
        ConfigUrl().base + ConfigUrl().unLikeMotivasi,
        data: {"idmotivasi": idmotivasi, "iduser": iduser},
        options: Options(
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}),
        // options: Options(headers: {'Content-type': 'application/json'}),
      );
      Logger().w(respon.data);
    }
  }

  Future putMotivasi(
      {required String judul,
      required String isi,
      required String tags,
      required List<File> datafile}) async {
    var token = '';
    var iduser = '';
    var namauser = '';
    await SharedPrefApp().prefLoadUser().then((value) {
      token = value[0];
      iduser = value[1];
      namauser = value[2];
    });
    dio.options.headers['Authorization'] = 'Bearer $token';
    List<MultipartFile> datafiles = []; // List file yang ingin dikirim

    for (var file in datafile.toList()) {
      String fileName = file.path;
      MultipartFile multipartFile =
          await MultipartFile.fromFile(file.path, filename: fileName);
      datafiles.add(multipartFile);
    }



    FormData formData = FormData.fromMap({
      "isi_motivasi": isi,
      "iduser": iduser,
      "judul": judul,
      "tags": tags,
      for (int i = 0; i < datafiles.length; i++)
        "gambar ${1 + i}": datafiles[i],
    });
    final respon = await dio.put(ConfigUrl().base + ConfigUrl().editMotivasi,
        data: formData);
    Logger().w(respon.data);

    return respon;
  }

  Future postMotivasi(
      {required String judul,
      required String isi,
      required String tags,
      required List<File> datafile}) async {
    var token = '';
    var iduser = '';
    var namauser = '';
    await SharedPrefApp().prefLoadUser().then((value) {
      token = value[0];
      iduser = value[1];
      namauser = value[2];
    });
    dio.options.headers['Authorization'] = 'Bearer $token';
    List<MultipartFile> datafiles = []; // List file yang ingin dikirim

    for (var file in datafile.toList()) {
      String fileName = file.path;
      MultipartFile multipartFile =
          await MultipartFile.fromFile(file.path, filename: fileName);
      datafiles.add(multipartFile);
    }

    List<Map<dynamic, dynamic>> kirimnew = [
      for (int i = 0; i < datafiles.length; i++)
        {'id': 1 + i, 'gambar${1 + i}': datafiles[i]},
    ];
    Logger().w(kirimnew);

    FormData formData = FormData.fromMap({
      "isi_motivasi": isi,
      "iduser": iduser,
      "judul": judul,
      "tags": tags,
      "nama_user": namauser,
      for (int i = 0; i < datafiles.length; i++)
        "gambar ${1 + i}": datafiles[i],
    });

    final respon = await dio.post(ConfigUrl().base + ConfigUrl().postMotivasi,
        data: formData);

    return respon;
  }
}
