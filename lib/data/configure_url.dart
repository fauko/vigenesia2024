class ConfigUrl {
  String base = "https://fauko.my.id/";
  String daftar = "api/registrasi";
  String login = "api/login";
  String user = "api/user";
  String editProfile = "api/PUTprofile";
  String getMotivasi = "api/Get_motivasi";
  String postMotivasi = "api/dev/POSTmotivasi";
  String editMotivasi = "api/dev/PUTmotivasi";
  String deleteMotivasi = "api/dev/DELETEmotivasi";
  String likeMotivasi = "api/dev/POSTlikemotivasi";
  String unLikeMotivasi = "api/dev/DELETElikemotivasi";
  String imagepick = "vigenesia/";
}
