import 'dart:async';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:geolocator/geolocator.dart';
import 'package:logger/logger.dart';
import 'package:vigenesia2024/data/configure_url.dart';
import 'package:vigenesia2024/domain/user/user.dart';
import 'package:vigenesia2024/helper/navigate.dart';
import 'package:vigenesia2024/helper/shared_pref.dart';
import 'package:vigenesia2024/helper/snackbar.dart';
import 'package:vigenesia2024/presentation/pages/auth/sign_in.dart';
import 'package:vigenesia2024/presentation/pages/main/home/home.dart';
import 'package:geocoding/geocoding.dart';

class Userdata {
  final dio = Dio();
  StreamController<List<User>> _userStreamController =
      StreamController<List<User>>.broadcast();

  Stream<List<User>> get userStream => _userStreamController.stream;
  bool whilecontinue = true;
  void initStreamController() {
    _userStreamController = StreamController<List<User>>.broadcast();
  }

  Future<void> dispose() async {
    whilecontinue = false;

    await _userStreamController.close();
  }

  Future<void> getDataAll() async {
    while (whilecontinue) {
      try {
        final response = await dio.get(ConfigUrl().base + ConfigUrl().user);
        List<dynamic> data = response.data;
        List<User> motivasiData = data.map((e) => User.fromJson(e)).toList();
        _userStreamController.add(motivasiData);
      } catch (e) {
        Logger().e(e);
      }

      Future.delayed(const Duration(seconds: 5));
    }
  }

  Future<void> editProfile(
      String nama, String profesi, File file, String password) async {
    MultipartFile gambar =
        await MultipartFile.fromFile(file.path, filename: file.path);
    var token = '';
    var email = '';
    var iduser = '';
    await SharedPrefApp().prefLoadUser().then((value) {
      token = value[0];
      iduser = value[1];
      email = value[3];
    });
    dio.options.headers['Authorization'] = 'Bearer $token';

    FormData formData = FormData.fromMap(
        nama.isNotEmpty && profesi.isNotEmpty && gambar.filename!.isNotEmpty
            ? {
                'iduser': iduser,
                'nama': nama,
                'profesi': profesi,
                "gambar": gambar,
                'email': email,
                'password': password,
              }
            : gambar.filename!.isNotEmpty
                ? {
                    "gambar": gambar,
                    'email': email,
                    'password': password,
                  }
                : nama.isNotEmpty && profesi.isNotEmpty
                    ? {
                        'nama': nama,
                        'profesi': profesi,
                      }
                    : nama.isNotEmpty && gambar.filename!.isNotEmpty
                        ? {
                            'nama': nama,
                            "gambar": gambar,
                          }
                        : profesi.isNotEmpty && gambar.filename!.isNotEmpty
                            ? {
                                'profesi': profesi,
                                "gambar": gambar,
                              }
                            : profesi.isNotEmpty
                                ? {
                                    'profesi': profesi,
                                  }
                                : {
                                    'nama': nama,
                                  });
    Logger().w(formData.fields);
    final respon = await dio.post(
      ConfigUrl().base + ConfigUrl().editProfile,
      data: formData,
      options: Options(headers: {'Content-type': 'application/json'}),
    );
    Logger().i(respon.data);
  }
}

class Auth {
  final dio = Dio();
  Future<void> daftar(
      {required BuildContext context,
      required String nama,
      required String profesi,
      required String email,
      required password}) async {
    final FormData formdat = FormData.fromMap({
      "nama": nama.toString(),
      "profesi": profesi.toString(),
      "email": email.toString(),
      "password": password.toString(),
    });
    final response = await dio.post(
      "${ConfigUrl().base}${ConfigUrl().daftar}",
      data: formdat,
    );
    response.statusCode == 200 &&
            response.data['message'] == "The user has been added successfully."
        ? Future.delayed(Duration.zero, () {
            NavigateApp().pushReplace(context, const SignInPage());
          })
        : Future.delayed(Duration.zero, () {
            SnackBarPopUp().snackbarEror(context, "Email Sudah terdaftar");
          });
  }

  Future<void> getCurrentLocation() async {
    Position position = await Geolocator.getCurrentPosition(
      desiredAccuracy: LocationAccuracy.high,
    );
    print('Latitude: ${position.latitude}, Longitude: ${position.longitude}');
  }

  Future<void> login({
    required BuildContext context,
    required String email,
    required String password,
    required String alamat,
  }) async {
    try {
      final FormData formdat = FormData.fromMap({
        "email": email.toString(),
        "password": password.toString(),
        "alamat": alamat,
      });
      final response = await dio.post(
        "${ConfigUrl().base}${ConfigUrl().login}",
        data: formdat,
      );
      Logger().w(response.data);
      await SharedPrefApp().prefUser(
        idUser: response.data['auth'][0]['iduser'],
        token: response.data['auth'][0]['token'],
        email: response.data['auth'][0]['email_user'],
        gambar: response.data['data']['gambar'].toString().isNotEmpty
            ? response.data['data']['gambar'].toString()
            : "kosong",
        nama: response.data['data']['nama'],
        profesi: response.data['data']['profesi'],
      );
      response.statusCode == 200 &&
              response.data['message'] == "User berhasil login kembali bro."
          ? Future.delayed(Duration.zero, () {
              NavigateApp().pushReplace(context, const HomeApp());
            })
          : Future.delayed(Duration.zero, () {
              SnackBarPopUp().snackbarEror(context, "Check email or password");
            });
    } catch (e) {
      Logger().e(e);
    }
  }
}
