import 'dart:io';

import 'package:flutter/material.dart';
import 'package:vigenesia2024/presentation/pages/main/main_app.dart';

void main() {
  runApp(const MainApp());
  HttpOverrides.global = MyHttpOverrides();
}

class MyHttpOverrides extends HttpOverrides {
  @override
  HttpClient createHttpClient(SecurityContext? context) {
    return super.createHttpClient(context)
      ..badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
  }
}
