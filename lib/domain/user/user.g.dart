// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$UserImpl _$$UserImplFromJson(Map<String, dynamic> json) => _$UserImpl(
      iduser: json['iduser'] as String,
      nama: json['nama'] as String,
      email: json['email'] as String,
      profesi: json['profesi'] as String,
      password: json['password'] as String,
      gambar: json['gambar'] as String,
    );

Map<String, dynamic> _$$UserImplToJson(_$UserImpl instance) =>
    <String, dynamic>{
      'iduser': instance.iduser,
      'nama': instance.nama,
      'email': instance.email,
      'profesi': instance.profesi,
      'password': instance.password,
      'gambar': instance.gambar,
    };
