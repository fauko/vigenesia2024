

import 'package:freezed_annotation/freezed_annotation.dart';



part 'user.freezed.dart';
part 'user.g.dart';

@freezed
class User with _$User {
 factory User(
     {
     required String iduser,
     required String nama,
     required String email,
     required String profesi,
     required String password,
     required String gambar,

     }) = _User;

 factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);
}