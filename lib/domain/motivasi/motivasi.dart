// ignore_for_file: invalid_annotation_target

import 'package:freezed_annotation/freezed_annotation.dart';

part 'motivasi.freezed.dart';
part 'motivasi.g.dart';

@freezed
class Motivasi with _$Motivasi {
  factory Motivasi({
    required String idmotivasi,
    required String judul,
    @JsonKey(name: 'isi_motivasi') required String isimotivasi,
    @JsonKey(name: 'nama_user') required String namauser,
    @JsonKey(name: 'tanggal_input') required String tanggalinput,
    @JsonKey(name: 'tanggal_update') required String tanggalupdate,
    required String tags,
    List? gambar,
    @JsonKey(name: 'like_motivasi') required int likemotivasi,
  }) = _Motivasi;

  factory Motivasi.fromJson(Map<String, dynamic> json) =>
      _$MotivasiFromJson(json);
}
