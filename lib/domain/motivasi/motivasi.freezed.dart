// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'motivasi.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

Motivasi _$MotivasiFromJson(Map<String, dynamic> json) {
  return _Motivasi.fromJson(json);
}

/// @nodoc
mixin _$Motivasi {
  String get idmotivasi => throw _privateConstructorUsedError;
  String get judul => throw _privateConstructorUsedError;
  @JsonKey(name: 'isi_motivasi')
  String get isimotivasi => throw _privateConstructorUsedError;
  @JsonKey(name: 'nama_user')
  String get namauser => throw _privateConstructorUsedError;
  @JsonKey(name: 'tanggal_input')
  String get tanggalinput => throw _privateConstructorUsedError;
  @JsonKey(name: 'tanggal_update')
  String get tanggalupdate => throw _privateConstructorUsedError;
  String get tags => throw _privateConstructorUsedError;
  List<dynamic>? get gambar => throw _privateConstructorUsedError;
  @JsonKey(name: 'like_motivasi')
  int get likemotivasi => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $MotivasiCopyWith<Motivasi> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $MotivasiCopyWith<$Res> {
  factory $MotivasiCopyWith(Motivasi value, $Res Function(Motivasi) then) =
      _$MotivasiCopyWithImpl<$Res, Motivasi>;
  @useResult
  $Res call(
      {String idmotivasi,
      String judul,
      @JsonKey(name: 'isi_motivasi') String isimotivasi,
      @JsonKey(name: 'nama_user') String namauser,
      @JsonKey(name: 'tanggal_input') String tanggalinput,
      @JsonKey(name: 'tanggal_update') String tanggalupdate,
      String tags,
      List<dynamic>? gambar,
      @JsonKey(name: 'like_motivasi') int likemotivasi});
}

/// @nodoc
class _$MotivasiCopyWithImpl<$Res, $Val extends Motivasi>
    implements $MotivasiCopyWith<$Res> {
  _$MotivasiCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? idmotivasi = null,
    Object? judul = null,
    Object? isimotivasi = null,
    Object? namauser = null,
    Object? tanggalinput = null,
    Object? tanggalupdate = null,
    Object? tags = null,
    Object? gambar = freezed,
    Object? likemotivasi = null,
  }) {
    return _then(_value.copyWith(
      idmotivasi: null == idmotivasi
          ? _value.idmotivasi
          : idmotivasi // ignore: cast_nullable_to_non_nullable
              as String,
      judul: null == judul
          ? _value.judul
          : judul // ignore: cast_nullable_to_non_nullable
              as String,
      isimotivasi: null == isimotivasi
          ? _value.isimotivasi
          : isimotivasi // ignore: cast_nullable_to_non_nullable
              as String,
      namauser: null == namauser
          ? _value.namauser
          : namauser // ignore: cast_nullable_to_non_nullable
              as String,
      tanggalinput: null == tanggalinput
          ? _value.tanggalinput
          : tanggalinput // ignore: cast_nullable_to_non_nullable
              as String,
      tanggalupdate: null == tanggalupdate
          ? _value.tanggalupdate
          : tanggalupdate // ignore: cast_nullable_to_non_nullable
              as String,
      tags: null == tags
          ? _value.tags
          : tags // ignore: cast_nullable_to_non_nullable
              as String,
      gambar: freezed == gambar
          ? _value.gambar
          : gambar // ignore: cast_nullable_to_non_nullable
              as List<dynamic>?,
      likemotivasi: null == likemotivasi
          ? _value.likemotivasi
          : likemotivasi // ignore: cast_nullable_to_non_nullable
              as int,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$MotivasiImplCopyWith<$Res>
    implements $MotivasiCopyWith<$Res> {
  factory _$$MotivasiImplCopyWith(
          _$MotivasiImpl value, $Res Function(_$MotivasiImpl) then) =
      __$$MotivasiImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {String idmotivasi,
      String judul,
      @JsonKey(name: 'isi_motivasi') String isimotivasi,
      @JsonKey(name: 'nama_user') String namauser,
      @JsonKey(name: 'tanggal_input') String tanggalinput,
      @JsonKey(name: 'tanggal_update') String tanggalupdate,
      String tags,
      List<dynamic>? gambar,
      @JsonKey(name: 'like_motivasi') int likemotivasi});
}

/// @nodoc
class __$$MotivasiImplCopyWithImpl<$Res>
    extends _$MotivasiCopyWithImpl<$Res, _$MotivasiImpl>
    implements _$$MotivasiImplCopyWith<$Res> {
  __$$MotivasiImplCopyWithImpl(
      _$MotivasiImpl _value, $Res Function(_$MotivasiImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? idmotivasi = null,
    Object? judul = null,
    Object? isimotivasi = null,
    Object? namauser = null,
    Object? tanggalinput = null,
    Object? tanggalupdate = null,
    Object? tags = null,
    Object? gambar = freezed,
    Object? likemotivasi = null,
  }) {
    return _then(_$MotivasiImpl(
      idmotivasi: null == idmotivasi
          ? _value.idmotivasi
          : idmotivasi // ignore: cast_nullable_to_non_nullable
              as String,
      judul: null == judul
          ? _value.judul
          : judul // ignore: cast_nullable_to_non_nullable
              as String,
      isimotivasi: null == isimotivasi
          ? _value.isimotivasi
          : isimotivasi // ignore: cast_nullable_to_non_nullable
              as String,
      namauser: null == namauser
          ? _value.namauser
          : namauser // ignore: cast_nullable_to_non_nullable
              as String,
      tanggalinput: null == tanggalinput
          ? _value.tanggalinput
          : tanggalinput // ignore: cast_nullable_to_non_nullable
              as String,
      tanggalupdate: null == tanggalupdate
          ? _value.tanggalupdate
          : tanggalupdate // ignore: cast_nullable_to_non_nullable
              as String,
      tags: null == tags
          ? _value.tags
          : tags // ignore: cast_nullable_to_non_nullable
              as String,
      gambar: freezed == gambar
          ? _value._gambar
          : gambar // ignore: cast_nullable_to_non_nullable
              as List<dynamic>?,
      likemotivasi: null == likemotivasi
          ? _value.likemotivasi
          : likemotivasi // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$MotivasiImpl implements _Motivasi {
  _$MotivasiImpl(
      {required this.idmotivasi,
      required this.judul,
      @JsonKey(name: 'isi_motivasi') required this.isimotivasi,
      @JsonKey(name: 'nama_user') required this.namauser,
      @JsonKey(name: 'tanggal_input') required this.tanggalinput,
      @JsonKey(name: 'tanggal_update') required this.tanggalupdate,
      required this.tags,
      final List<dynamic>? gambar,
      @JsonKey(name: 'like_motivasi') required this.likemotivasi})
      : _gambar = gambar;

  factory _$MotivasiImpl.fromJson(Map<String, dynamic> json) =>
      _$$MotivasiImplFromJson(json);

  @override
  final String idmotivasi;
  @override
  final String judul;
  @override
  @JsonKey(name: 'isi_motivasi')
  final String isimotivasi;
  @override
  @JsonKey(name: 'nama_user')
  final String namauser;
  @override
  @JsonKey(name: 'tanggal_input')
  final String tanggalinput;
  @override
  @JsonKey(name: 'tanggal_update')
  final String tanggalupdate;
  @override
  final String tags;
  final List<dynamic>? _gambar;
  @override
  List<dynamic>? get gambar {
    final value = _gambar;
    if (value == null) return null;
    if (_gambar is EqualUnmodifiableListView) return _gambar;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(value);
  }

  @override
  @JsonKey(name: 'like_motivasi')
  final int likemotivasi;

  @override
  String toString() {
    return 'Motivasi(idmotivasi: $idmotivasi, judul: $judul, isimotivasi: $isimotivasi, namauser: $namauser, tanggalinput: $tanggalinput, tanggalupdate: $tanggalupdate, tags: $tags, gambar: $gambar, likemotivasi: $likemotivasi)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$MotivasiImpl &&
            (identical(other.idmotivasi, idmotivasi) ||
                other.idmotivasi == idmotivasi) &&
            (identical(other.judul, judul) || other.judul == judul) &&
            (identical(other.isimotivasi, isimotivasi) ||
                other.isimotivasi == isimotivasi) &&
            (identical(other.namauser, namauser) ||
                other.namauser == namauser) &&
            (identical(other.tanggalinput, tanggalinput) ||
                other.tanggalinput == tanggalinput) &&
            (identical(other.tanggalupdate, tanggalupdate) ||
                other.tanggalupdate == tanggalupdate) &&
            (identical(other.tags, tags) || other.tags == tags) &&
            const DeepCollectionEquality().equals(other._gambar, _gambar) &&
            (identical(other.likemotivasi, likemotivasi) ||
                other.likemotivasi == likemotivasi));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      idmotivasi,
      judul,
      isimotivasi,
      namauser,
      tanggalinput,
      tanggalupdate,
      tags,
      const DeepCollectionEquality().hash(_gambar),
      likemotivasi);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$MotivasiImplCopyWith<_$MotivasiImpl> get copyWith =>
      __$$MotivasiImplCopyWithImpl<_$MotivasiImpl>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$MotivasiImplToJson(
      this,
    );
  }
}

abstract class _Motivasi implements Motivasi {
  factory _Motivasi(
          {required final String idmotivasi,
          required final String judul,
          @JsonKey(name: 'isi_motivasi') required final String isimotivasi,
          @JsonKey(name: 'nama_user') required final String namauser,
          @JsonKey(name: 'tanggal_input') required final String tanggalinput,
          @JsonKey(name: 'tanggal_update') required final String tanggalupdate,
          required final String tags,
          final List<dynamic>? gambar,
          @JsonKey(name: 'like_motivasi') required final int likemotivasi}) =
      _$MotivasiImpl;

  factory _Motivasi.fromJson(Map<String, dynamic> json) =
      _$MotivasiImpl.fromJson;

  @override
  String get idmotivasi;
  @override
  String get judul;
  @override
  @JsonKey(name: 'isi_motivasi')
  String get isimotivasi;
  @override
  @JsonKey(name: 'nama_user')
  String get namauser;
  @override
  @JsonKey(name: 'tanggal_input')
  String get tanggalinput;
  @override
  @JsonKey(name: 'tanggal_update')
  String get tanggalupdate;
  @override
  String get tags;
  @override
  List<dynamic>? get gambar;
  @override
  @JsonKey(name: 'like_motivasi')
  int get likemotivasi;
  @override
  @JsonKey(ignore: true)
  _$$MotivasiImplCopyWith<_$MotivasiImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
