// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'motivasi.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$MotivasiImpl _$$MotivasiImplFromJson(Map<String, dynamic> json) =>
    _$MotivasiImpl(
      idmotivasi: json['idmotivasi'] as String,
      judul: json['judul'] as String,
      isimotivasi: json['isi_motivasi'] as String,
      namauser: json['nama_user'] as String,
      tanggalinput: json['tanggal_input'] as String,
      tanggalupdate: json['tanggal_update'] as String,
      tags: json['tags'] as String,
      gambar: json['gambar'] as List<dynamic>?,
      likemotivasi: (json['like_motivasi'] as num).toInt(),
    );

Map<String, dynamic> _$$MotivasiImplToJson(_$MotivasiImpl instance) =>
    <String, dynamic>{
      'idmotivasi': instance.idmotivasi,
      'judul': instance.judul,
      'isi_motivasi': instance.isimotivasi,
      'nama_user': instance.namauser,
      'tanggal_input': instance.tanggalinput,
      'tanggal_update': instance.tanggalupdate,
      'tags': instance.tags,
      'gambar': instance.gambar,
      'like_motivasi': instance.likemotivasi,
    };
