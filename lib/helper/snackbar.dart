import 'package:animated_snack_bar/animated_snack_bar.dart';
import 'package:flutter/material.dart';

class SnackBarPopUp {
  void snackbarInfo(BuildContext context, String text) {
    AnimatedSnackBar.material(
      text,
      type: AnimatedSnackBarType.info,
      mobileSnackBarPosition: MobileSnackBarPosition.bottom,
      duration: const Duration(milliseconds: 1500),
      desktopSnackBarPosition: DesktopSnackBarPosition.topCenter,
      snackBarStrategy: RemoveSnackBarStrategy(),
    ).show(context);
  }

  void snackbarEror(BuildContext context, String text) {
    AnimatedSnackBar.material(
      text,
      type: AnimatedSnackBarType.error,
      mobileSnackBarPosition: MobileSnackBarPosition.bottom,
      desktopSnackBarPosition: DesktopSnackBarPosition.topCenter,
      snackBarStrategy: RemoveSnackBarStrategy(),
    ).show(context);
  }

  void snackbarSucces(BuildContext context, String text) {
    AnimatedSnackBar.material(
      text,
      type: AnimatedSnackBarType.success,
      mobileSnackBarPosition: MobileSnackBarPosition.bottom,
      desktopSnackBarPosition: DesktopSnackBarPosition.topCenter,
      snackBarStrategy: RemoveSnackBarStrategy(),
    ).show(context);
  }

  void showLeftSheet(BuildContext context, Widget widget) {
    showGeneralDialog(
      context: context,
      barrierDismissible: true,
      barrierLabel: MaterialLocalizations.of(context).modalBarrierDismissLabel,
      barrierColor: Colors.black.withOpacity(0.5),
      transitionDuration: const Duration(milliseconds: 200),
      pageBuilder: (BuildContext context, Animation animation,
          Animation secondaryAnimation) {
        return Align(
          alignment:
              Alignment.centerLeft, // Atur posisi bottom sheet di sisi kiri
          child: widget,
        );
      },
    );
  }
}
