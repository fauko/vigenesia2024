import 'package:shared_preferences/shared_preferences.dart';

class SharedPrefApp {
  prefUser(
      {
        required String token,
      required String idUser,
      required String nama,
      required String email,
      required String profesi,
      required String gambar}) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();

    await preferences.setStringList('authUser', [
      token,
      idUser,
      nama,
      profesi,
      email,
      gambar,
    ]);
  }

  Future prefLoadUser() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    return preferences.getStringList('authUser');
  }

  Future prefRemoveUser() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.remove('authUser');
  }
}
